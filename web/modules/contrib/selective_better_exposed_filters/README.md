# Selective Better Exposed Filters

Provide extra option for better exposed filters to show only used terms in filter. This module is very simple and just add a part of fuctionality of Views Selective Filters (https://www.drupal.org/project/views_selective_filters) to Better Exposed Filters module. Module provide checkbox "Show only used terms" and work only with Taxonomy term reference fields.

## Installation
It's very easy. You need just enable module and change settings of Better Exposed Filter style in your view.

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229), use Composer to
download the module:
   ```sh
   composer require "drupal/selective_better_exposed_filters"
   ```
