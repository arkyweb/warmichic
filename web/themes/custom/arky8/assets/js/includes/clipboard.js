var clip = new Clipboard('#clipboard');

clip.on("success", function() {
  document.getElementById('clipboard').insertAdjacentHTML('beforeend', '<div class="tooltip">Ya copio la URL.</div>');
});
clip.on("error", function() {
  document.getElementById('clipboard').insertAdjacentHTML('beforeend', '<div class="tooltip">No copio la URL</div>');
});