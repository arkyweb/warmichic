(function ($) {
  'use strict';

  Drupal.behaviors.arky_swiper = {
    attach: function(context, settings) {
      var swiper = new Swiper('.swiper-container', {
                  slidesPerView: 1,
                  spaceBetween: 0,
                  loop: false,
                  pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                  },
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  },
                }); 

      jQuery('.button--add-to-cart').attr('value','¡Compralo ahora!');
    }
  };

}(jQuery));