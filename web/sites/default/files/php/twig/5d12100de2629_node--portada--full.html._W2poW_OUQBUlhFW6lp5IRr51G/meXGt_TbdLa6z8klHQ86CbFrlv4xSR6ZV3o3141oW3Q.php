<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--portada--full.html.twig */
class __TwigTemplate_a6b87a23e63e31ac5d69e51367cb9fbfed6585fca6110a85375ca1b6f2fbab5e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 5, "if" => 29, "for" => 39];
        $filters = ["escape" => 24, "length" => 29, "t" => 37, "first" => 39];
        $functions = ["file_url" => 7];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'length', 't', 'first'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        $context["PORTADA_SLOGAN_VALUE"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_slogan", []), "value", []);
        // line 6
        echo "
";
        // line 7
        $context["PORTADA_BG"] = call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_img", []), "entity", []), "fileuri", []))]);
        // line 8
        echo "
";
        // line 9
        $context["PORTADA_AD_MAIN"] = $this->getAttribute(($context["content"] ?? null), "field_portada_ad_main", []);
        // line 10
        $context["PORTADA_AD_MAIN_EXIST"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_ad_main", []), "value", []);
        // line 11
        echo "
";
        // line 12
        $context["PORTADA_AD_SEC"] = $this->getAttribute(($context["content"] ?? null), "field_portada_ad_sec", []);
        // line 13
        $context["PORTADA_AD_SEC_EXIST"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_ad_sec", []), "value", []);
        // line 14
        echo "
";
        // line 15
        $context["PORTADA_AD_SIMPLE"] = $this->getAttribute(($context["content"] ?? null), "field_portada_ad_simple", []);
        // line 16
        $context["PORTADA_AD_SIMPLE_EXIST"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_ad_simple", []), "value", []);
        // line 17
        echo "
";
        // line 18
        $context["PORTADA_AD_SLOGAN"] = $this->getAttribute(($context["content"] ?? null), "field_portada_ad_slogan", []);
        // line 19
        $context["PORTADA_AD_SLOGAN_EXIST"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_ad_slogan", []), "value", []);
        // line 20
        echo "
";
        // line 24
        echo "<section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "mainpage-content"], "method")), "html", null, true);
        echo "> 

";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

";
        // line 29
        if ((twig_length_filter($this->env, ($context["PORTADA_SLOGAN_VALUE"] ?? null)) > 0)) {
            // line 30
            echo "<div class=\"banner-message\">
    <h2>";
            // line 31
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["PORTADA_SLOGAN_VALUE"] ?? null)), "html", null, true);
            echo "</h2>
</div>
";
        }
        // line 34
        echo "
";
        // line 35
        if ((twig_length_filter($this->env, ($context["PORTADA_AD_MAIN_EXIST"] ?? null)) > 0)) {
            // line 36
            echo "<div class=\"block banner-powerful\">
<h4 class=\"title-block-display text-center\">";
            // line 37
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Nuestras colecciones"));
            echo "</h4>
<ul>
    ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["PORTADA_AD_MAIN"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    // line 40
                    echo "    <li class=\"item-0";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                    echo "</li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "    ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_portada_img", []), 0, [], "array"), "value", [])) > 0)) {
                // line 43
                echo "        <span class=\"image\" style=\"background-image: url( ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["PORTADA_BG"] ?? null)), "html", null, true);
                echo " )\">
    ";
            }
            // line 44
            echo " 
</ul>
</div>
";
        }
        // line 48
        echo "
";
        // line 49
        if ((twig_length_filter($this->env, ($context["PORTADA_AD_SEC_EXIST"] ?? null)) > 0)) {
            // line 50
            echo "<div class=\"block banner-powerful-more grids-for-2 block-70\">
<h4 class=\"title-block-display\">";
            // line 51
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("También tenemos"));
            echo "</h4>
<ul class=\"grids\">
    ";
            // line 53
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["PORTADA_AD_SEC"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    // line 54
                    echo "    <li class=\"item-0";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                    echo "</li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "</ul>
</div>
";
        }
        // line 59
        echo "
";
        // line 60
        if ((twig_length_filter($this->env, ($context["PORTADA_AD_SIMPLE_EXIST"] ?? null)) > 0)) {
            // line 61
            echo "<div class=\"block banner-powerful-more grids-for-4\">
<h4 class=\"title-block-display\">";
            // line 62
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Y más"));
            echo "</h4>
<ul class=\"grids\">
    ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["PORTADA_AD_SIMPLE"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    // line 65
                    echo "    <li class=\"item-0";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                    echo "</li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "</ul>
</div>
";
        }
        // line 70
        echo "
";
        // line 71
        if ((twig_length_filter($this->env, ($context["PORTADA_AD_SLOGAN_EXIST"] ?? null)) > 0)) {
            // line 72
            echo "<div class=\"block-slogans\"> 
    ";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["PORTADA_AD_SLOGAN"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    // line 74
                    echo "    <article class=\"slogans slogans-0";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                    echo "</article>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo " 
</div>
";
        }
        // line 77
        echo " 

</section> ";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--portada--full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 77,  298 => 75,  283 => 74,  272 => 73,  269 => 72,  267 => 71,  264 => 70,  259 => 67,  244 => 65,  233 => 64,  228 => 62,  225 => 61,  223 => 60,  220 => 59,  215 => 56,  200 => 54,  189 => 53,  184 => 51,  181 => 50,  179 => 49,  176 => 48,  170 => 44,  164 => 43,  161 => 42,  146 => 40,  135 => 39,  130 => 37,  127 => 36,  125 => 35,  122 => 34,  116 => 31,  113 => 30,  111 => 29,  106 => 27,  102 => 26,  96 => 24,  93 => 20,  91 => 19,  89 => 18,  86 => 17,  84 => 16,  82 => 15,  79 => 14,  77 => 13,  75 => 12,  72 => 11,  70 => 10,  68 => 9,  65 => 8,  63 => 7,  60 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# VARIABLES PARA HOMEPAGE PRINCIPAL \t\t#}
{# ---------------------------------------- #}

{% set PORTADA_SLOGAN_VALUE = node.field_slogan.value %}

{% set PORTADA_BG = file_url(node.field_portada_img.entity.fileuri) %}

{% set PORTADA_AD_MAIN = content.field_portada_ad_main %}
{% set PORTADA_AD_MAIN_EXIST = node.field_portada_ad_main.value %}

{% set PORTADA_AD_SEC = content.field_portada_ad_sec %}
{% set PORTADA_AD_SEC_EXIST = node.field_portada_ad_sec.value %}

{% set PORTADA_AD_SIMPLE = content.field_portada_ad_simple %}
{% set PORTADA_AD_SIMPLE_EXIST = node.field_portada_ad_simple.value %}

{% set PORTADA_AD_SLOGAN = content.field_portada_ad_slogan %}
{% set PORTADA_AD_SLOGAN_EXIST = node.field_portada_ad_slogan.value %}

{# ---------------------------------------- #}
{# EMPIEZA EL NODO DE PAGINA \t\t\t\t#}
{# ---------------------------------------- #}
<section{{ attributes.addClass('mainpage-content') }}> 

{{ title_prefix }} 
{{ title_suffix }}

{% if PORTADA_SLOGAN_VALUE|length > 0 %}
<div class=\"banner-message\">
    <h2>{{ PORTADA_SLOGAN_VALUE }}</h2>
</div>
{% endif %}

{% if PORTADA_AD_MAIN_EXIST|length > 0 %}
<div class=\"block banner-powerful\">
<h4 class=\"title-block-display text-center\">{{ 'Nuestras colecciones'|t }}</h4>
<ul>
    {% for key, item in PORTADA_AD_MAIN if key|first != '#' %}
    <li class=\"item-0{{ loop.index }}\">{{ item }}</li>
    {% endfor %}
    {% if node.field_portada_img[0].value|length > 0 %}
        <span class=\"image\" style=\"background-image: url( {{ PORTADA_BG }} )\">
    {% endif %} 
</ul>
</div>
{% endif %}

{% if PORTADA_AD_SEC_EXIST|length > 0 %}
<div class=\"block banner-powerful-more grids-for-2 block-70\">
<h4 class=\"title-block-display\">{{ 'También tenemos'|t }}</h4>
<ul class=\"grids\">
    {% for key, item in PORTADA_AD_SEC if key|first != '#' %}
    <li class=\"item-0{{ loop.index }}\">{{ item }}</li>
    {% endfor %}
</ul>
</div>
{% endif %}

{% if PORTADA_AD_SIMPLE_EXIST|length > 0 %}
<div class=\"block banner-powerful-more grids-for-4\">
<h4 class=\"title-block-display\">{{ 'Y más'|t }}</h4>
<ul class=\"grids\">
    {% for key, item in PORTADA_AD_SIMPLE if key|first != '#' %}
    <li class=\"item-0{{ loop.index }}\">{{ item }}</li>
    {% endfor %}
</ul>
</div>
{% endif %}

{% if PORTADA_AD_SLOGAN_EXIST|length > 0 %}
<div class=\"block-slogans\"> 
    {% for key, item in PORTADA_AD_SLOGAN if key|first != '#' %}
    <article class=\"slogans slogans-0{{ loop.index }}\">{{ item }}</article>
    {% endfor %} 
</div>
{% endif %} 

</section> {# CULMINA EL DIV DEL NODE #}", "themes/custom/arky8/templates/content/node--portada--full.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/content/node--portada--full.html.twig");
    }
}
