<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/layout/page--checkout.html.twig */
class __TwigTemplate_e9c80a0cae4e92db3fb031bad8c2120225f6e33821f8da3e166855df0f249c5a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 6];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/custom"), "html", null, true);
        echo "
";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/style_tricky"), "html", null, true);
        echo "

<section class=\"arkyweb-section\">

\t";
        // line 6
        $this->loadTemplate("@arky8/-regions/header.html.twig", "themes/custom/arky8/templates/layout/page--checkout.html.twig", 6)->display($context);
        // line 7
        echo "
\t";
        // line 8
        $this->loadTemplate("@arky8/-regions/close.html.twig", "themes/custom/arky8/templates/layout/page--checkout.html.twig", 8)->display($context);
        // line 9
        echo "
\t";
        // line 10
        $this->loadTemplate("@arky8/-regions/content-simple.html.twig", "themes/custom/arky8/templates/layout/page--checkout.html.twig", 10)->display($context);
        // line 11
        echo "
    ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
    ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "

</section> ";
        // line 16
        echo "
";
        // line 17
        $this->loadTemplate("@arky8/-regions/footer.html.twig", "themes/custom/arky8/templates/layout/page--checkout.html.twig", 17)->display($context);
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/layout/page--checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 17,  90 => 16,  85 => 13,  81 => 12,  78 => 11,  76 => 10,  73 => 9,  71 => 8,  68 => 7,  66 => 6,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{{ attach_library('arky8/custom') }}
{{ attach_library('arky8/style_tricky') }}

<section class=\"arkyweb-section\">

\t{% include '@arky8/-regions/header.html.twig' %}

\t{% include '@arky8/-regions/close.html.twig' %}

\t{% include '@arky8/-regions/content-simple.html.twig' %}

    {{ page.sidebar_first }}
    {{ page.sidebar_second }}

</section> {# Acaba SECTION #}

{% include '@arky8/-regions/footer.html.twig' %}", "themes/custom/arky8/templates/layout/page--checkout.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/layout/page--checkout.html.twig");
    }
}
