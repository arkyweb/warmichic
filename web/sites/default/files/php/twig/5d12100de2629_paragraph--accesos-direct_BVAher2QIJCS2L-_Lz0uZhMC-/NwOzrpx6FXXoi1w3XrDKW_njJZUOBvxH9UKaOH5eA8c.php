<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--slogan.html.twig */
class __TwigTemplate_655ceee11ff5c7ce83947fc9f9a7731eafaa8cb43da85e17cc7535e906b9bb26 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 3];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "
";
        // line 3
        $context["LINK"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_link", []), 0, []), "url", []);
        // line 4
        $context["TEXT_UP"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_txt_up", []), "value", []);
        // line 5
        $context["TEXT_DOWN"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_txt_down", []), "value", []);
        echo " 

<a href=\"";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["LINK"] ?? null)), "html", null, true);
        echo "\" title=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_DOWN"] ?? null)), "html", null, true);
        echo "\" alt=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_DOWN"] ?? null)), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo "> 

";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

    <span>";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_UP"] ?? null)), "html", null, true);
        echo "</span>
    <small>";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_DOWN"] ?? null)), "html", null, true);
        echo "</small> 
</a>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--slogan.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 13,  87 => 12,  82 => 10,  78 => 9,  67 => 7,  62 => 5,  60 => 4,  58 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# VARIABLES #}

{% set LINK = paragraph.field_ad_link.0.url %}
{% set TEXT_UP = paragraph.field_ad_txt_up.value %}
{% set TEXT_DOWN = paragraph.field_ad_txt_down.value %} 

<a href=\"{{ LINK }}\" title=\"{{ TEXT_DOWN }}\" alt=\"{{ TEXT_DOWN }}\" {{ attributes }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <span>{{ TEXT_UP }}</span>
    <small>{{ TEXT_DOWN }}</small> 
</a>", "themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--slogan.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--slogan.html.twig");
    }
}
