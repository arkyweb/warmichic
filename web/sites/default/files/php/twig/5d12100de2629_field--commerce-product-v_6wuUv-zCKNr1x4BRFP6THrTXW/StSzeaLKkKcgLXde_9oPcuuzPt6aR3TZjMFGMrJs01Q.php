<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/commerce/field--commerce-product-variation--field-product-var-img--mis-warmis.html.twig */
class __TwigTemplate_a1608628c9f878114f04c6aae663df63c7a3804f299ae011acd6a318a0c6a22b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 4, "if" => 19, "for" => 26];
        $filters = ["escape" => 1, "clean_class" => 6, "image_style" => 34];
        $functions = ["attach_library" => 1, "file_url" => 29];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'clean_class', 'image_style'],
                ['attach_library', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/inlcude_swiper"), "html", null, true);
        echo "

";
        // line 4
        $context["classes"] = [0 => "field", 1 => ("field--name-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 6
($context["field_name"] ?? null)))), 2 => ("field--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 7
($context["field_type"] ?? null)))), 3 => ("field--label-" . $this->sandbox->ensureToStringAllowed(        // line 8
($context["label_display"] ?? null))), 4 => "swiper-container"];
        // line 13
        $context["title_classes"] = [0 => "field--label", 1 => (((        // line 15
($context["label_display"] ?? null) == "visually_hidden")) ? ("sr-only") : (""))];
        // line 18
        echo "
";
        // line 19
        if (($context["label_hidden"] ?? null)) {
            // line 20
            if (($context["multiple"] ?? null)) {
                // line 21
                echo "
<div";
                // line 22
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null), 1 => "field--items"], "method")), "html", null, true);
                echo ">

  <div class=\"swiper-wrapper my-gallery\" itemscope itemtype=\"http://schema.org/ImageGallery\">

    ";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 27
                    echo "      <figure class=\"swiper-slide\" itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">
        <a 
          href=\"";
                    // line 29
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "entity", []), "uri", []), "value", []))]), "html", null, true);
                    echo "\" 
          data-size=\"";
                    // line 30
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "width", [])), "html", null, true);
                    echo "x";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "height", [])), "html", null, true);
                    echo "\" 
          itemprop=\"contentUrl\"
        >
          <img 
            src=\"";
                    // line 34
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\value\Twig\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "entity", []), "uri", []), "value", [])), "800x800")]), "html", null, true);
                    echo "\" 
            title=\"";
                    // line 35
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "title", [])), "html", null, true);
                    echo "\" 
            alt=\"";
                    // line 36
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "content", []), "#item", [], "array"), "alt", [])), "html", null, true);
                    echo "\" 
            itemprop=\"thumbnail\" 
          />
        </a>
      </figure>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo " 

  </div>

  <div class=\"swiper-pagination\"></div>
  <div class=\"swiper-button-prev\"></div>
  <div class=\"swiper-button-next\"></div>
        
  </div>

<div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
    <div class=\"pswp__bg\"></div>
    <div class=\"pswp__scroll-wrap\">
        <div class=\"pswp__container\">
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
        </div>
        <div class=\"pswp__ui pswp__ui--hidden\">
            <div class=\"pswp__top-bar\">
                <div class=\"pswp__counter\"></div>
                <button class=\"pswp__button pswp__button--close\"></button>
                <button class=\"pswp__button pswp__button--share\"></button>
                <button class=\"pswp__button pswp__button--fs\"></button>
                <button class=\"pswp__button pswp__button--zoom\"></button>
                <div class=\"pswp__preloader\">
                    <div class=\"pswp__preloader__icn\">
                        <div class=\"pswp__preloader__cut\">
                            <div class=\"pswp__preloader__donut\"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
                <div class=\"pswp__share-tooltip\"></div>
            </div>
            <button class=\"pswp__button pswp__button--arrow--left\"></button>
            <button class=\"pswp__button pswp__button--arrow--right\"></button>
            <div class=\"pswp__caption\">
                <div class=\"pswp__caption__center\"></div>
            </div>
        </div>
    </div>
</div>

";
            }
        }
        // line 87
        echo " 

<style>
  .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background-color: transparent;  
        display: flex; 
        justify-content: center; 
        align-items: center;
        flex-direction: column;
    }
    .swiper-slide a {
        display: block;
        height: 100%;
    }
    .swiper-container img {
        max-width: 100%;
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: center;
    }
</style>

";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/include_photoswipe"), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/commerce/field--commerce-product-variation--field-product-var-img--mis-warmis.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 116,  175 => 87,  126 => 41,  114 => 36,  110 => 35,  106 => 34,  97 => 30,  93 => 29,  89 => 27,  85 => 26,  78 => 22,  75 => 21,  73 => 20,  71 => 19,  68 => 18,  66 => 15,  65 => 13,  63 => 8,  62 => 7,  61 => 6,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{{ attach_library( 'arky8/inlcude_swiper' ) }}

{%
  set classes = [
    'field',
    'field--name-' ~ field_name|clean_class,
    'field--type-' ~ field_type|clean_class,
    'field--label-' ~ label_display,
    'swiper-container',
  ]
%}
{%
  set title_classes = [
    'field--label',
    label_display == 'visually_hidden' ? 'sr-only',
  ]
%}

{% if label_hidden %}
{% if multiple %}

<div{{ attributes.addClass(classes, 'field--items') }}>

  <div class=\"swiper-wrapper my-gallery\" itemscope itemtype=\"http://schema.org/ImageGallery\">

    {% for item in items %}
      <figure class=\"swiper-slide\" itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">
        <a 
          href=\"{{ file_url(item.content['#item'].entity.uri.value) }}\" 
          data-size=\"{{ item.content['#item'].width }}x{{ item.content['#item'].height }}\" 
          itemprop=\"contentUrl\"
        >
          <img 
            src=\"{{ file_url(item.content['#item'].entity.uri.value|image_style('800x800')) }}\" 
            title=\"{{ item.content['#item'].title }}\" 
            alt=\"{{ item.content['#item'].alt }}\" 
            itemprop=\"thumbnail\" 
          />
        </a>
      </figure>
    {% endfor %} 

  </div>

  <div class=\"swiper-pagination\"></div>
  <div class=\"swiper-button-prev\"></div>
  <div class=\"swiper-button-next\"></div>
        
  </div>

<div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
    <div class=\"pswp__bg\"></div>
    <div class=\"pswp__scroll-wrap\">
        <div class=\"pswp__container\">
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
        </div>
        <div class=\"pswp__ui pswp__ui--hidden\">
            <div class=\"pswp__top-bar\">
                <div class=\"pswp__counter\"></div>
                <button class=\"pswp__button pswp__button--close\"></button>
                <button class=\"pswp__button pswp__button--share\"></button>
                <button class=\"pswp__button pswp__button--fs\"></button>
                <button class=\"pswp__button pswp__button--zoom\"></button>
                <div class=\"pswp__preloader\">
                    <div class=\"pswp__preloader__icn\">
                        <div class=\"pswp__preloader__cut\">
                            <div class=\"pswp__preloader__donut\"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
                <div class=\"pswp__share-tooltip\"></div>
            </div>
            <button class=\"pswp__button pswp__button--arrow--left\"></button>
            <button class=\"pswp__button pswp__button--arrow--right\"></button>
            <div class=\"pswp__caption\">
                <div class=\"pswp__caption__center\"></div>
            </div>
        </div>
    </div>
</div>

{% endif %}
{% endif %} 

<style>
  .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background-color: transparent;  
        display: flex; 
        justify-content: center; 
        align-items: center;
        flex-direction: column;
    }
    .swiper-slide a {
        display: block;
        height: 100%;
    }
    .swiper-container img {
        max-width: 100%;
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: center;
    }
</style>

{{ attach_library( 'arky8/include_photoswipe' )}}", "themes/custom/arky8/templates/commerce/field--commerce-product-variation--field-product-var-img--mis-warmis.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/commerce/field--commerce-product-variation--field-product-var-img--mis-warmis.html.twig");
    }
}
