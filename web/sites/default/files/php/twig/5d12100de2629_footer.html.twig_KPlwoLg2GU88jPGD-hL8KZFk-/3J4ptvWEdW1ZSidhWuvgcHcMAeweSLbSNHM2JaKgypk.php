<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @arky8/-regions/footer.html.twig */
class __TwigTemplate_e87395495543d149c4e75d0543d636b06759b574e5bee05bbca7a2c329600692 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 2
            echo "<footer class=\"arkyweb-footer\">
    <div class=\"container\">
        ";
            // line 4
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
        <div class=\"block by-arkyweb text-center\">
            <a href=\"https://arkyweb.com\" target=\"_blank\" rel=\"follow\">
                Diseño Web y Página Web hechos por <strong>Arkyweb</strong> en Lima, Perú
            </a>
        </div>
    </div>
</footer>
";
        }
    }

    public function getTemplateName()
    {
        return "@arky8/-regions/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 4,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if page.footer %}
<footer class=\"arkyweb-footer\">
    <div class=\"container\">
        {{ page.footer }}
        <div class=\"block by-arkyweb text-center\">
            <a href=\"https://arkyweb.com\" target=\"_blank\" rel=\"follow\">
                Diseño Web y Página Web hechos por <strong>Arkyweb</strong> en Lima, Perú
            </a>
        </div>
    </div>
</footer>
{% endif %}", "@arky8/-regions/footer.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/-regions/footer.html.twig");
    }
}
