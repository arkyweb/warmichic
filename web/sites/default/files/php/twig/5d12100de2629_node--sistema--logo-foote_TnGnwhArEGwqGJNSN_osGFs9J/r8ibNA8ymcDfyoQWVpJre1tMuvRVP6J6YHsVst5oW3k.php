<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--sistema--logo-footer.html.twig */
class __TwigTemplate_a7ef431ed97c194684b8aaf9e0627ac3e86649181f0db890e0c725fae91f1e55 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 5];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        $context["LOGO"] = $this->getAttribute(($context["content"] ?? null), "field_logo", []);
        // line 6
        echo "
<div style=\"display: contents\" ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">

";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

<h5 class=\"logo\"> 
    <a href=\"/\" title=\"Warmichic\">
        ";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["LOGO"] ?? null)), "html", null, true);
        echo "
    </a> 
</h5>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--sistema--logo-footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 14,  72 => 10,  68 => 9,  63 => 7,  60 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# VARIABLES PARA SISTEMA SEGURIDAD         #}
{# ---------------------------------------- #}

{% set LOGO = content.field_logo %}

<div style=\"display: contents\" {{ attributes }}>

{{ title_prefix }} 
{{ title_suffix }}

<h5 class=\"logo\"> 
    <a href=\"/\" title=\"Warmichic\">
        {{ LOGO }}
    </a> 
</h5>

</div>{# CULMINA EL DIV DEL NODE #}", "themes/custom/arky8/templates/content/node--sistema--logo-footer.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/content/node--sistema--logo-footer.html.twig");
    }
}
