<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--sistema--seguridad.html.twig */
class __TwigTemplate_41d911422f5654968a94725dfb3536df215c05ce85d930f66444f2b0834b15d6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 5, "if" => 13, "for" => 16];
        $filters = ["escape" => 8, "length" => 13, "t" => 14, "first" => 16];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'length', 't', 'first'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        $context["SISTEMA_SEGURIDAD"] = $this->getAttribute(($context["content"] ?? null), "field_sistema_seguridad", []);
        // line 6
        $context["SISTEMA_SEGURIDAD_EXIST"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_sistema_seguridad", []), "value", []);
        // line 7
        echo "
<div style=\"display: contents\" ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">

";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

";
        // line 13
        if ((twig_length_filter($this->env, ($context["SISTEMA_SEGURIDAD_EXIST"] ?? null)) > 0)) {
            // line 14
            echo "<h4 class=\"title-block\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Comprar en Warmichic es muy fácil y seguro"));
            echo "</h4>
<ul class=\"grids\">
    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["SISTEMA_SEGURIDAD"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    // line 17
                    echo "    <li class=\"card-simple item-0";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                    echo "</li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "</ul>
";
        }
        // line 21
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--sistema--seguridad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 21,  113 => 19,  98 => 17,  87 => 16,  81 => 14,  79 => 13,  74 => 11,  70 => 10,  65 => 8,  62 => 7,  60 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# VARIABLES PARA SISTEMA SEGURIDAD         #}
{# ---------------------------------------- #}

{% set SISTEMA_SEGURIDAD = content.field_sistema_seguridad %}
{% set SISTEMA_SEGURIDAD_EXIST = node.field_sistema_seguridad.value %}

<div style=\"display: contents\" {{ attributes }}>

{{ title_prefix }} 
{{ title_suffix }}

{% if SISTEMA_SEGURIDAD_EXIST|length > 0 %}
<h4 class=\"title-block\">{{ 'Comprar en Warmichic es muy fácil y seguro'|t }}</h4>
<ul class=\"grids\">
    {% for key, item in SISTEMA_SEGURIDAD if key|first != '#' %}
    <li class=\"card-simple item-0{{ loop.index }}\">{{ item }}</li>
    {% endfor %}
</ul>
{% endif %}

</div>{# CULMINA EL DIV DEL NODE #}", "themes/custom/arky8/templates/content/node--sistema--seguridad.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/content/node--sistema--seguridad.html.twig");
    }
}
