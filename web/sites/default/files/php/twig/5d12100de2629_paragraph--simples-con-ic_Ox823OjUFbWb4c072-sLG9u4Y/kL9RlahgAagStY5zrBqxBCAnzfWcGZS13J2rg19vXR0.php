<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraphs/paragraph--simples-con-icono--icon.html.twig */
class __TwigTemplate_5e2bf7fde51a38ef7e8a8dcb5f27071ed22324179dff55c1f7498e1bc87e89fe extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 3];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "
";
        // line 3
        $context["TITLE"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_sic_title", []), "value", []);
        // line 4
        $context["CUERPO"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_sic_body", []), "value", []);
        // line 5
        $context["ICONO"] = $this->getAttribute(($context["content"] ?? null), "field_sic_svg", []);
        // line 6
        echo "
<div style=\"display: contents\" ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
    ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
    ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
    <figure>
        ";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ICONO"] ?? null)), "html", null, true);
        echo "
    </figure>
    <h4>";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TITLE"] ?? null)), "html", null, true);
        echo "</h4>
    <p>";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["CUERPO"] ?? null)), "html", null, true);
        echo "</p>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraphs/paragraph--simples-con-icono--icon.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 14,  85 => 13,  80 => 11,  75 => 9,  71 => 8,  67 => 7,  64 => 6,  62 => 5,  60 => 4,  58 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# VARIABLES #}

{% set TITLE = paragraph.field_sic_title.value %}
{% set CUERPO = paragraph.field_sic_body.value %}
{% set ICONO = content.field_sic_svg %}

<div style=\"display: contents\" {{ attributes }}>
    {{ title_prefix }} 
    {{ title_suffix }}
    <figure>
        {{ ICONO }}
    </figure>
    <h4>{{ TITLE }}</h4>
    <p>{{ CUERPO }}</p>
</div>", "themes/custom/arky8/templates/paragraphs/paragraph--simples-con-icono--icon.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/paragraphs/paragraph--simples-con-icono--icon.html.twig");
    }
}
