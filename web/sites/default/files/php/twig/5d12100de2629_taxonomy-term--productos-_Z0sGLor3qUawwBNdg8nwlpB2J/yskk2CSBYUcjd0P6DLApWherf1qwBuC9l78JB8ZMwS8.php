<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/taxonomy-term--productos-taxonomy.html.twig */
class __TwigTemplate_190ff033ee43a8dc2a2cb746cf4c720607cd6ff49a834854cc028e352dd598ac extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 6, "if" => 14];
        $filters = ["escape" => 9, "raw" => 25];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 5
        echo "
";
        // line 6
        $context["TITULO"] = ($context["name"] ?? null);
        // line 7
        $context["DESCRIPTION"] = $this->getAttribute($this->getAttribute(($context["term"] ?? null), "description", []), "value", []);
        // line 8
        echo "
<div class=\"taxonomy-head\" style=\"display: contents\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
  
  ";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
  ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

    ";
        // line 14
        if (($this->getAttribute($this->getAttribute($this->getAttribute(($context["term"] ?? null), "parent", []), 0, []), "target_id", []) == 1)) {
            // line 15
            echo "        <h3 class=\"title-term-sub\">Colección</h3>
    ";
        } elseif (($this->getAttribute($this->getAttribute($this->getAttribute(        // line 16
($context["term"] ?? null), "parent", []), 0, []), "target_id", []) == 2)) {
            // line 17
            echo "        <h3 class=\"title-term-sub\">Casuales</h3>
    ";
        } elseif (($this->getAttribute($this->getAttribute($this->getAttribute(        // line 18
($context["term"] ?? null), "parent", []), 0, []), "target_id", []) == 3)) {
            // line 19
            echo "        <h3 class=\"title-term-sub\">Accesorios</h3>
    ";
        } else {
            // line 21
            echo "    ";
        }
        // line 22
        echo "
    <h1 id=\"title\" class=\"title-term\">";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TITULO"] ?? null)), "html", null, true);
        echo "</h1>

    <div class=\"description-term\">";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["DESCRIPTION"] ?? null)));
        echo "</div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/taxonomy-term--productos-taxonomy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 25,  101 => 23,  98 => 22,  95 => 21,  91 => 19,  89 => 18,  86 => 17,  84 => 16,  81 => 15,  79 => 14,  74 => 12,  70 => 11,  65 => 9,  62 => 8,  60 => 7,  58 => 6,  55 => 5,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# VARIABLES PARA TAXONOMY term             #}
{# {{ content }} {{ content.description }}  #}
{# ---------------------------------------- #}

{% set TITULO = name %}
{% set DESCRIPTION = term.description.value %}

<div class=\"taxonomy-head\" style=\"display: contents\"{{ attributes }}>
  
  {{ title_prefix }}
  {{ title_suffix }}

    {% if term.parent.0.target_id == 1 %}
        <h3 class=\"title-term-sub\">Colección</h3>
    {% elseif term.parent.0.target_id == 2 %}
        <h3 class=\"title-term-sub\">Casuales</h3>
    {% elseif term.parent.0.target_id == 3 %}
        <h3 class=\"title-term-sub\">Accesorios</h3>
    {% else %}
    {% endif %}

    <h1 id=\"title\" class=\"title-term\">{{ TITULO }}</h1>

    <div class=\"description-term\">{{ DESCRIPTION|raw }}</div>

</div>", "themes/custom/arky8/templates/content/taxonomy-term--productos-taxonomy.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/content/taxonomy-term--productos-taxonomy.html.twig");
    }
}
