<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @arky8/-regions/content-full.html.twig */
class __TwigTemplate_7fbbd5068d95f42ca73baff138ce4e67d2f14003d3cf8d81f0e5afe33c30c42f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 7];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "    <main id=\"mainpage\" class=\"mainpage\">
        <div class=\"container container-right\">

            ";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "

            ";
        // line 7
        if ($this->getAttribute(($context["page"] ?? null), "pc", [])) {
            // line 8
            echo "            <section class=\"mainpage-postcontent\">
                ";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pc", [])), "html", null, true);
            echo "
            </section>
            ";
        }
        // line 12
        echo "
        </div>
    </main>";
    }

    public function getTemplateName()
    {
        return "@arky8/-regions/content-full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 12,  70 => 9,  67 => 8,  65 => 7,  60 => 5,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# MIANPAGE: Content + PostContent #}
    <main id=\"mainpage\" class=\"mainpage\">
        <div class=\"container container-right\">

            {{ page.content }}

            {% if page.pc %}
            <section class=\"mainpage-postcontent\">
                {{ page.pc }}
            </section>
            {% endif %}

        </div>
    </main>", "@arky8/-regions/content-full.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/-regions/content-full.html.twig");
    }
}
