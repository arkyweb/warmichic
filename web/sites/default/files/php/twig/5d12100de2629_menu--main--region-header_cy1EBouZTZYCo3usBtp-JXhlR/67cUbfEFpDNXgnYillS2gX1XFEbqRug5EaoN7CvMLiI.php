<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/menu/menu--main--region-header.html.twig */
class __TwigTemplate_db98f3ae9b90a16c0acbbae85fbfe15044a28d5a783e56af6369d32efa3a4665 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 22, "macro" => 29, "set" => 33, "if" => 45, "for" => 53];
        $filters = ["escape" => 48, "keys" => 55];
        $functions = ["menus_attribute" => 55, "link" => 98];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'set', 'if', 'for'],
                ['escape', 'keys'],
                ['menus_attribute', 'link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 21
        echo "
";
        // line 22
        $context["menus"] = $this;
        // line 23
        echo "
";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0));
        echo "
";
    }

    // line 29
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            echo "  
  ";
            // line 30
            $context["menus"] = $this;
            // line 31
            echo "
";
            // line 33
            $context["menu_classes"] = [0 => "menu", 1 => "menu-header"];
            // line 38
            echo "  ";
            // line 39
            echo "  ";
            // line 40
            $context["submenu_classes"] = [0 => "menu-sub"];
            // line 44
            echo "
  ";
            // line 45
            if (($context["items"] ?? null)) {
                // line 46
                echo "
    ";
                // line 47
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 48
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["menu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                } else {
                    // line 50
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "removeClass", [0 => ($context["menu_classes"] ?? null)], "method"), "addClass", [0 => ($context["submenu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                }
                // line 52
                echo "
    ";
                // line 53
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 54
                    echo "
      ";
                    // line 55
                    $context["menu_attributes"] = $this->env->getExtension('Drupal\menus_attribute\Template\TwigExtension')->menusAttribute($this->sandbox->ensureToStringAllowed($this->getAttribute(twig_get_array_keys_filter($this->sandbox->ensureToStringAllowed(($context["items"] ?? null))), $this->getAttribute($context["loop"], "index0", []), [], "array")));
                    // line 56
                    echo "      ";
                    // line 57
                    $context["classes"] = [0 => "menu-item", 1 => (($this->getAttribute(                    // line 59
$context["item"], "is_expanded", [])) ? ("menu-item--expanded") : ("")), 2 => (($this->getAttribute(                    // line 60
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed") : ("")), 3 => (($this->getAttribute(                    // line 61
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 62
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 65
                    echo "      ";
                    // line 66
                    $context["link_classes"] = [0 => "menu-link", 1 => (($this->getAttribute(                    // line 68
$context["item"], "is_expanded", [])) ? ("dropdown") : (""))];
                    // line 71
                    echo "
      ";
                    // line 73
                    $context["classes_sub"] = [0 => "menu-item-sub", 1 => (($this->getAttribute(                    // line 75
$context["item"], "is_expanded", [])) ? ("menu-item--expanded-sub") : ("")), 2 => (($this->getAttribute(                    // line 76
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed-sub") : ("")), 3 => (($this->getAttribute(                    // line 77
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail-sub") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 78
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 81
                    echo "      ";
                    // line 82
                    $context["link_classes_sub"] = [0 => "menu-link-sub"];
                    // line 86
                    echo "
      ";
                    // line 87
                    if ((($context["menu_level"] ?? null) == 0)) {
                        // line 88
                        echo "
      <li data-counter=\"menu-item-0";
                        // line 89
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 91
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 92
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 94
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 95
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 97
                        echo "      >
        ";
                        // line 98
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => ($context["link_classes"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                        echo "

        ";
                        // line 100
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 101
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        }
                        // line 103
                        echo "
      </li>

      ";
                    } else {
                        // line 107
                        echo "
      <li data-counter=\"menu-item-sub-0";
                        // line 108
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes_sub"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 110
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 111
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 113
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 114
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 116
                        echo "      >
        ";
                        // line 117
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes_sub"] ?? null)], "method"), "addClass", [0 => ($context["link_classes_sub"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                        echo "

        ";
                        // line 119
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 120
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        }
                        // line 122
                        echo "
      </li>

      ";
                    }
                    // line 126
                    echo "
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 128
                echo "
    </ul>
  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/menu/menu--main--region-header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 128,  274 => 126,  268 => 122,  262 => 120,  260 => 119,  255 => 117,  252 => 116,  246 => 114,  243 => 113,  237 => 111,  235 => 110,  228 => 108,  225 => 107,  219 => 103,  213 => 101,  211 => 100,  206 => 98,  203 => 97,  197 => 95,  194 => 94,  188 => 92,  186 => 91,  179 => 89,  176 => 88,  174 => 87,  171 => 86,  169 => 82,  167 => 81,  165 => 78,  164 => 77,  163 => 76,  162 => 75,  161 => 73,  158 => 71,  156 => 68,  155 => 66,  153 => 65,  151 => 62,  150 => 61,  149 => 60,  148 => 59,  147 => 57,  145 => 56,  143 => 55,  140 => 54,  123 => 53,  120 => 52,  114 => 50,  108 => 48,  106 => 47,  103 => 46,  101 => 45,  98 => 44,  96 => 40,  94 => 39,  92 => 38,  90 => 33,  87 => 31,  85 => 30,  69 => 29,  63 => 28,  60 => 23,  58 => 22,  55 => 21,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *   - is_expanded: TRUE if the link has visible children within the current
 *     menu tree.
 *   - is_collapsed: TRUE if the link has children within the current menu tree
 *     that are not currently visible.
 *   - in_active_trail: TRUE if the link is in the active trail.
 */
#}

{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}
{{ menus.menu_links(items, attributes, 0) }}
{% macro menu_links(items, attributes, menu_level) %}  
  {% import _self as menus %}

{%
    set menu_classes = [
      'menu',
      'menu-header', 
    ]
  %}
  {# 1. #}
  {%
    set submenu_classes = [
      'menu-sub', 
    ]
%}

  {% if items %}

    {% if menu_level == 0 %}
      <ul{{ attributes.addClass(menu_classes) }}>
    {% else %}
      <ul{{ attributes.removeClass(menu_classes).addClass(submenu_classes) }}>
    {% endif %}

    {% for item in items %}

      {% set menu_attributes = menus_attribute(items|keys[loop.index0]) %}
      {%
        set classes = [
          'menu-item',
          item.is_expanded ? 'menu-item--expanded',
          item.is_collapsed ? 'menu-item--collapsed',
          item.in_active_trail ? 'menu-item--active-trail',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes = [
          'menu-link',
          item.is_expanded ? 'dropdown',
        ]
      %}

      {%
        set classes_sub = [
          'menu-item-sub',
          item.is_expanded ? 'menu-item--expanded-sub',
          item.is_collapsed ? 'menu-item--collapsed-sub',
          item.in_active_trail ? 'menu-item--active-trail-sub',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes_sub = [
          'menu-link-sub',
        ]
      %}

      {% if menu_level == 0 %}

      <li data-counter=\"menu-item-0{{ loop.index }}\" {{ item.attributes.addClass(classes) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >
        {{ link(item.title, item.url, item.attributes.removeClass(classes).addClass(link_classes), menu_attributes.link) }}

        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}

      </li>

      {% else %}

      <li data-counter=\"menu-item-sub-0{{ loop.index }}\" {{ item.attributes.addClass(classes_sub) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >
        {{ link(item.title, item.url, item.attributes.removeClass(classes_sub).addClass(link_classes_sub), menu_attributes.link) }}

        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}

      </li>

      {% endif %}

    {% endfor %}

    </ul>
  {% endif %}
{% endmacro %}", "themes/custom/arky8/templates/menu/menu--main--region-header.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/menu/menu--main--region-header.html.twig");
    }
}
