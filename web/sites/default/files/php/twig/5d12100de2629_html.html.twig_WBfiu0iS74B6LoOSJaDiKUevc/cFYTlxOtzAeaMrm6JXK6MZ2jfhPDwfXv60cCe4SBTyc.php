<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/layout/html.html.twig */
class __TwigTemplate_0f12854f90021704129a9fe6f8ba8a731f3d3457ef4393811a2a77c61c940ccf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "include" => 32];
        $filters = ["clean_class" => 4, "escape" => 20, "raw" => 23, "safe_join" => 24];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'include'],
                ['clean_class', 'escape', 'raw', 'safe_join'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["body_classes"] = [0 => "site", 1 => (( !        // line 4
($context["root_path"] ?? null)) ? ("home") : (("inside-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["root_path"] ?? null)))))), 2 => ((        // line 5
($context["language"] ?? null)) ? (("lang-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["language"] ?? null))))) : ("")), 3 => (( !        // line 6
($context["logged_in"] ?? null)) ? ("off") : ("in")), 4 => (( !        // line 7
($context["node_type"] ?? null)) ? ("config") : (("node-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["node_type"] ?? null)))))), 5 => ((        // line 8
($context["db_offline"] ?? null)) ? ("offline") : ("")), 6 => ((        // line 9
($context["term_id"] ?? null)) ? (("taxo-" . $this->sandbox->ensureToStringAllowed(($context["term_id"] ?? null)))) : ("")), 7 => (($this->getAttribute(        // line 10
($context["path_info"] ?? null), "args", [])) ? (("path-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["path_info"] ?? null), "args", [])))) : ("")), 8 => ((        // line 11
($context["current_path"] ?? null)) ? (("current" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["current_path"] ?? null))))) : ("")), 9 => ((( !$this->getAttribute(        // line 12
($context["page"] ?? null), "sidebar_first", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-0") : ("")), 10 => ((($this->getAttribute(        // line 13
($context["page"] ?? null), "sidebar_first", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-1") : ("")), 11 => ((($this->getAttribute(        // line 14
($context["page"] ?? null), "sidebar_second", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_first", []))) ? ("cols-2") : ("")), 12 => ((($this->getAttribute(        // line 15
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-x") : (""))];
        // line 18
        echo "
<!DOCTYPE html>
<html ";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["html_attributes"] ?? null)), "html", null, true);
        echo ">

<head>
  <head-placeholder token=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  <title>";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null)), " | "));
        echo "</title>
  <meta property=\"fb:app_id\" content=\"\"> 

</head>

<body id=\"site\" ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["body_classes"] ?? null)], "method")), "html", null, true);
        echo ">

  <style media=\"all\">
    ";
        // line 32
        $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = null;
        try {
            $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 =             $this->loadTemplate((($context["directory"] ?? null) . "/templates/style/inline.css"), "themes/custom/arky8/templates/layout/html.html.twig", 32);
        } catch (LoaderError $e) {
            // ignore missing template
        }
        if ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) {
            $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4->display($context);
        }
        // line 33
        echo "  </style> 

  ";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null)), "html", null, true);
        echo "
  ";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null)), "html", null, true);
        echo " 
  ";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null)), "html", null, true);
        echo "

  <css-placeholder token=\"";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">

  <js-placeholder token=\"";
        // line 41
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  <js-bottom-placeholder token=\"";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">

  <script>
    ";
        // line 45
        $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = null;
        try {
            $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 =             $this->loadTemplate((($context["directory"] ?? null) . "/templates/script/font.js"), "themes/custom/arky8/templates/layout/html.html.twig", 45);
        } catch (LoaderError $e) {
            // ignore missing template
        }
        if ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) {
            $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144->display($context);
        }
        // line 46
        echo "  </script>

  <span class=\"dot\"></span>

</body>

</html>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/layout/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 46,  139 => 45,  133 => 42,  129 => 41,  124 => 39,  119 => 37,  115 => 36,  111 => 35,  107 => 33,  97 => 32,  91 => 29,  83 => 24,  79 => 23,  73 => 20,  69 => 18,  67 => 15,  66 => 14,  65 => 13,  64 => 12,  63 => 11,  62 => 10,  61 => 9,  60 => 8,  59 => 7,  58 => 6,  57 => 5,  56 => 4,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set body_classes = [
    'site',
    not root_path ? 'home' : 'inside-' ~ root_path|clean_class,
    language ? 'lang-' ~ language|clean_class,
    not logged_in ? 'off' : 'in',
    not node_type ? 'config' : 'node-' ~ node_type|clean_class,
    db_offline ? 'offline',
    term_id ? 'taxo-' ~ term_id, 
    path_info.args ? 'path-' ~ path_info.args,
    current_path ? 'current' ~ current_path|clean_class,
    not page.sidebar_first and not page.sidebar_second ? 'cols-0',
    page.sidebar_first and not page.sidebar_second ? 'cols-1',
    page.sidebar_second and not page.sidebar_first ? 'cols-2',
    page.sidebar_first and page.sidebar_second ? 'cols-x'
  ]
%}

<!DOCTYPE html>
<html {{ html_attributes }}>

<head>
  <head-placeholder token=\"{{ placeholder_token|raw }}\">
  <title>{{ head_title|safe_join(' | ') }}</title>
  <meta property=\"fb:app_id\" content=\"\"> 

</head>

<body id=\"site\" {{ attributes.addClass(body_classes) }}>

  <style media=\"all\">
    {% include directory ~ '/templates/style/inline.css' ignore missing %}
  </style> 

  {{ page_top }}
  {{ page }} 
  {{ page_bottom }}

  <css-placeholder token=\"{{ placeholder_token|raw }}\">

  <js-placeholder token=\"{{ placeholder_token|raw }}\">
  <js-bottom-placeholder token=\"{{ placeholder_token|raw }}\">

  <script>
    {% include directory ~ '/templates/script/font.js' ignore missing %}
  </script>

  <span class=\"dot\"></span>

</body>

</html>", "themes/custom/arky8/templates/layout/html.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/layout/html.html.twig");
    }
}
