<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/commerce/commerce-cart-block.html.twig */
class __TwigTemplate_a55e8e488892587e7ed19e482705539142811b8c0ccb780c9fd7fcfec825db4f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 7];
        $filters = ["escape" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
  <div class=\"cart-block--summary\">
    <a class=\"carrito-link cart-block--link__expand\" href=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null)), "html", null, true);
        echo "\">
      <span class=\"cart-block--summary__icon\">
<svg viewBox=\"0 0 51.2 56.3\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m46.8 13.4c0-1.5-1.2-2.7-2.7-2.7-11.7 0.1-22.4 0.5-28.7 1.2v-0.3l-0.1-2.3-0.1-1.2c0-0.5-0.1-1.1-0.3-1.6-0.6-1.9-2.2-3.5-4.1-4.1-0.4-0.1-0.8-0.1-1.3-0.2h-1.2l-1.7 0.1c-1.1 0.1-2.1 0.2-3 0.3-0.3 0-0.4 0.7-0.4 1.4s0.2 1.2 0.4 1.2c0.9 0.1 1.9 0.2 3 0.3l1.7 0.2c0.2 0 0.4 0 0.7 0.1 0.2 0 0.3 0.1 0.5 0.2 0.7 0.3 1.1 1 1.3 1.7v0.5 1l-0.1 2.3c-0.2 6.3-0.3 13.4-0.4 20.6v1.3c0 0.6 0 1.2 0.1 1.7 0.6 4 3.9 7 7.9 7.3h1.5 1.2l2.5-0.1 4.8-0.2c6.3-0.2 11.6-0.6 15.2-1 0.3 0 0.4-0.7 0.4-1.5s-0.2-1.3-0.4-1.3c-3.6-0.5-8.9-0.8-15.2-1l-4.8-0.2-2.5 0.2h-1.3-1.1c-1-0.1-1.8-0.6-2.4-1.4-0.3-0.4-0.4-0.8-0.5-1.3-0.1-0.3 0-1.4 0-2.3v-2c4.2 0.5 10.4 0.8 17.4 1l5.9 0.1h0.8 0.9c2.8-0.2 5.1-2.1 5.8-4.8 0.1-0.4 0.2-0.9 0.2-1.3v-0.5-5.1l0.1-6.3zm-6 12.6c-0.1 0.1-0.3 0.1-0.4 0.1h-0.6-0.8l-5.9 0.2c-7 0.2-13.2 0.6-17.4 1 0-4.3-0.1-8.4-0.2-12.3 5.8 0.6 15.3 1 25.9 1.2v9.2c-0.1 0.2-0.3 0.4-0.6 0.6z\"/><circle cx=\"21.8\" cy=\"50.3\" r=\"3.2\"/><circle cx=\"35.7\" cy=\"50.3\" r=\"3.2\"/></svg>
      </span>
      ";
        // line 7
        if ((($context["count"] ?? null) > 1)) {
            // line 8
            echo "        <span class=\"cart-block--summary__count\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["count"] ?? null)), "html", null, true);
            echo "</span>
      ";
        } else {
            // line 10
            echo "        <span class=\"cart-block--summary__count\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["count"] ?? null)), "html", null, true);
            echo "</span>
      ";
        }
        // line 12
        echo "    </a>
  </div>
  ";
        // line 14
        if (($context["content"] ?? null)) {
            // line 15
            echo "  <div class=\"cart-block--contents\">
    <div class=\"cart-block--contents__inner\">
      <div class=\"cart-block--contents__items\">
        ";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
            echo "
      </div>
      <div class=\"cart-block--contents__links\">
        ";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["links"] ?? null)), "html", null, true);
            echo "
      </div>
    </div>
  </div>
  ";
        }
        // line 26
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/commerce/commerce-cart-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 26,  99 => 21,  93 => 18,  88 => 15,  86 => 14,  82 => 12,  76 => 10,  70 => 8,  68 => 7,  61 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div{{ attributes}}>
  <div class=\"cart-block--summary\">
    <a class=\"carrito-link cart-block--link__expand\" href=\"{{ url }}\">
      <span class=\"cart-block--summary__icon\">
<svg viewBox=\"0 0 51.2 56.3\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m46.8 13.4c0-1.5-1.2-2.7-2.7-2.7-11.7 0.1-22.4 0.5-28.7 1.2v-0.3l-0.1-2.3-0.1-1.2c0-0.5-0.1-1.1-0.3-1.6-0.6-1.9-2.2-3.5-4.1-4.1-0.4-0.1-0.8-0.1-1.3-0.2h-1.2l-1.7 0.1c-1.1 0.1-2.1 0.2-3 0.3-0.3 0-0.4 0.7-0.4 1.4s0.2 1.2 0.4 1.2c0.9 0.1 1.9 0.2 3 0.3l1.7 0.2c0.2 0 0.4 0 0.7 0.1 0.2 0 0.3 0.1 0.5 0.2 0.7 0.3 1.1 1 1.3 1.7v0.5 1l-0.1 2.3c-0.2 6.3-0.3 13.4-0.4 20.6v1.3c0 0.6 0 1.2 0.1 1.7 0.6 4 3.9 7 7.9 7.3h1.5 1.2l2.5-0.1 4.8-0.2c6.3-0.2 11.6-0.6 15.2-1 0.3 0 0.4-0.7 0.4-1.5s-0.2-1.3-0.4-1.3c-3.6-0.5-8.9-0.8-15.2-1l-4.8-0.2-2.5 0.2h-1.3-1.1c-1-0.1-1.8-0.6-2.4-1.4-0.3-0.4-0.4-0.8-0.5-1.3-0.1-0.3 0-1.4 0-2.3v-2c4.2 0.5 10.4 0.8 17.4 1l5.9 0.1h0.8 0.9c2.8-0.2 5.1-2.1 5.8-4.8 0.1-0.4 0.2-0.9 0.2-1.3v-0.5-5.1l0.1-6.3zm-6 12.6c-0.1 0.1-0.3 0.1-0.4 0.1h-0.6-0.8l-5.9 0.2c-7 0.2-13.2 0.6-17.4 1 0-4.3-0.1-8.4-0.2-12.3 5.8 0.6 15.3 1 25.9 1.2v9.2c-0.1 0.2-0.3 0.4-0.6 0.6z\"/><circle cx=\"21.8\" cy=\"50.3\" r=\"3.2\"/><circle cx=\"35.7\" cy=\"50.3\" r=\"3.2\"/></svg>
      </span>
      {% if count > 1 %}
        <span class=\"cart-block--summary__count\">{{ count }}</span>
      {% else %}
        <span class=\"cart-block--summary__count\">{{ count }}</span>
      {% endif %}
    </a>
  </div>
  {% if content %}
  <div class=\"cart-block--contents\">
    <div class=\"cart-block--contents__inner\">
      <div class=\"cart-block--contents__items\">
        {{ content }}
      </div>
      <div class=\"cart-block--contents__links\">
        {{ links }}
      </div>
    </div>
  </div>
  {% endif %}
</div>
", "themes/custom/arky8/templates/commerce/commerce-cart-block.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/commerce/commerce-cart-block.html.twig");
    }
}
