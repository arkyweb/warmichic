<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--default.html.twig */
class __TwigTemplate_fc17f59894109cdadcd1f429d23f5a31dcab00517c172844a54e9ff7cdded5d3 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 3];
        $filters = ["escape" => 7];
        $functions = ["file_url" => 5];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "
";
        // line 3
        $context["LINK"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_link", []), 0, []), "url", []);
        // line 4
        $context["TEXT_UP"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_txt_up", []), "value", []);
        // line 5
        $context["IMAGE_URL"] = call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_ad_img", []), "entity", []), "fileuri", []))]);
        // line 6
        echo "
<a href=\"";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["LINK"] ?? null)), "html", null, true);
        echo "\" title=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_UP"] ?? null)), "html", null, true);
        echo "\" alt=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_UP"] ?? null)), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "powerful-colection"], "method")), "html", null, true);
        echo ">
    ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
    ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
    <figure>
        <img src=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["IMAGE_URL"] ?? null)), "html", null, true);
        echo "\" />
    </figure>
    <h3>";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TEXT_UP"] ?? null)), "html", null, true);
        echo "</h3>
</a>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 13,  86 => 11,  81 => 9,  77 => 8,  67 => 7,  64 => 6,  62 => 5,  60 => 4,  58 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# VARIABLES #}

{% set LINK = paragraph.field_ad_link.0.url %}
{% set TEXT_UP = paragraph.field_ad_txt_up.value %}
{% set IMAGE_URL = file_url(paragraph.field_ad_img.entity.fileuri) %}

<a href=\"{{ LINK }}\" title=\"{{ TEXT_UP }}\" alt=\"{{ TEXT_UP }}\" {{ attributes.addClass('powerful-colection') }}>
    {{ title_prefix }} 
    {{ title_suffix }}
    <figure>
        <img src=\"{{ IMAGE_URL }}\" />
    </figure>
    <h3>{{ TEXT_UP }}</h3>
</a>", "themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--default.html.twig", "/home4/arkyweb/live/warmipage/web/themes/custom/arky8/templates/paragraphs/paragraph--accesos-directos--default.html.twig");
    }
}
